package com.example.hellotest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.example.hellotest.LoginFragment.Companion.PARAM_KEY
import com.example.hellotest.home.HomeActivity
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity(), MainView {
    override fun onClick(data: String) {
        Toast.makeText(this, data, Toast.LENGTH_SHORT ).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.v("MainActivity", "onCreate")

        Picasso.get().load("http://i.imgur.com/DvpvklR.png")
            .into(imageTest)
        val bundle = Bundle()
        bundle.putString(PARAM_KEY, "PRIMEIRO")
        val fragment = LoginFragment.newInstance(bundle)

        val bundle2 = Bundle()
        bundle2.putString(PARAM_KEY, "segundo")

        val fragment2 = LoginFragment.newInstance(bundle2)
        supportFragmentManager.
                beginTransaction()
            .add(R.id.fragment1, fragment)
            .commit()

        supportFragmentManager.
            beginTransaction()
            .add(R.id.fragment2, fragment2)
            .commit()
    }

    override fun onStart() {
        super.onStart()

        Log.v("MainActivity", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.v("MainActivity", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.v("MainActivity", "onPause")

    }


    override fun onStop() {
        super.onStop()
        Log.v("MainActivity", "onStop")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v("MainActivity", "onDestroy")

    }
}
