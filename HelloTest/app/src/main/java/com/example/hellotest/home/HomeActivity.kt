package com.example.hellotest.home

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.hellotest.R

class HomeActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        Log.v("HomeActivity", "onCreate")

    }

    override fun onStart() {
        super.onStart()

        Log.v("HomeActivity", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.v("HomeActivity", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.v("HomeActivity", "onPause")

    }


    override fun onStop() {
        super.onStop()
        Log.v("HomeActivity", "onStop")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v("HomeActivity", "onDestroy")

    }
}