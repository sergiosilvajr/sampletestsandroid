package com.example.hellotest

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment: Fragment() {

    lateinit var mainView: MainView

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            mainView = context as MainView
        } catch (e: Exception){

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.activity as? MainActivity
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        val text = view.findViewById<EditText>(R.id.texto1)

        arguments?.let{
            text.hint = it.get(PARAM_KEY).toString()
        }
        val button = view.findViewById<Button>(R.id.frag_button)
        button.setOnClickListener {
            mainView.onClick(texto1.text.toString())
        }
        return view
    }

    companion object {
        const val PARAM_KEY = "parameter"

        fun newInstance(bundle: Bundle? = null): LoginFragment {
            var loginFragment = LoginFragment()
            loginFragment.arguments = bundle
            return loginFragment
        }

        fun newInstance(data: String): LoginFragment {
            var loginFragment = LoginFragment()
            val bundle = Bundle()
            bundle.putString(PARAM_KEY, data)
            loginFragment.arguments = bundle
            return loginFragment
        }
    }


}